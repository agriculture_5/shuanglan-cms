import Layout from '@/layout'

const productRouter = {
  path: '/fittings',
  component: Layout,
  redirect: '/fittings/list',
  name: 'Fittings',
  meta: {
    title: 'fittings',
    icon: 'example',
  },
  children: [
    {
      path: 'list',
      component: () => import('@/views/product/list'),
      name: 'FittingsListManagement',
      meta: { title: 'fittingsListManagement', noCache: true },
    },
    {
      path: 'create',
      component: () => import('@/views/product/create'),
      name: 'FittingsCreate',
      breadcrumb: false,
      meta: { title: 'fittingsCreate', noCache: true },
    },
    {
      path: 'update',
      component: () => import('@/views/product/update'),
      hidden: true,
      name: 'FittingsUpdate',
      breadcrumb: false,
      meta: { title: 'fittingsUpdate', noCache: true },
    },
    // 分类
    {
      path: 'fittingsCategory/list',
      component: () => import('@/views/product/category/list'),
      name: 'FittingsCategoryManagement',
      meta: { title: 'FittingsCategoryManagement', noCache: true },
    },
    {
      path: 'fittingsCategory/create',
      component: () => import('@/views/product/category/create'),
      hidden: true,
      name: 'FittingsCategoryCreate',
      breadcrumb: false,
      meta: { title: 'fittingsCategoryCreate', noCache: true },
    },
    {
      path: 'fittingsCategory/update',
      component: () => import('@/views/product/category/update'),
      hidden: true,
      name: 'FittingsCategoryUpdate',
      breadcrumb: false,
      meta: { title: 'fittingsCategoryUpdate', noCache: true },
    },
  ],
}
export default productRouter
