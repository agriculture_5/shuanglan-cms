import Layout from '@/layout'

const futureRouter = {
  path: '/future',
  component: Layout,
  redirect: '/future/list',
  name: 'Future',
  meta: {
    title: 'future',
    icon: 'example',
  },
  children: [
    {
      path: 'list',
      component: () => import('@/views/future/list'),
      name: 'FutureListManagement',
      meta: { title: 'futureListManagement', noCache: true },
    },
    {
      path: 'create',
      component: () => import('@/views/future/create'),
      name: 'FutureCreate',
      breadcrumb: false,
      meta: { title: 'futureCreate', noCache: true },
    },
    {
      path: 'update',
      component: () => import('@/views/future/update'),
      hidden: true,
      name: 'FutureUpdate',
      breadcrumb: false,
      meta: { title: 'futureUpdate', noCache: true },
    },

    // 分类
    {
      path: 'futureCategory/list',
      component: () => import('@/views/future/category/list'),
      name: 'FutureCategoryManagement',
      meta: { title: 'futureCategoryManagement', noCache: true },
    },
    {
      path: 'futureCategory/create',
      component: () => import('@/views/future/category/create'),
      hidden: true,
      name: 'FutureCategoryCreate',
      breadcrumb: false,
      meta: { title: 'futureCategoryCreate', noCache: true },
    },
    {
      path: 'futureCategory/update',
      component: () => import('@/views/future/category/update'),
      hidden: true,
      name: 'FutureCategoryUpdate',
      breadcrumb: false,
      meta: { title: 'futureCategoryUpdate', noCache: true },
    },
  ],
}
export default futureRouter
