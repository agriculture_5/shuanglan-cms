import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('future-categories')
export class ProductCategories {
  @PrimaryGeneratedColumn('uuid')
  id: number;

  @Column()
  name: string;


  @Column('mediumtext', { nullable: true })
  description: string;

  @Column()
  status: boolean;

  @Column({
    select: true,
  })
  createdAt: Date;

  @Column()
  updatedAt: Date;
}
