const baseHosts = {
  // development
  development: {
    baseHost: 'http://localhost:8037/',
    uploadPath: 'public/',
    domain: '双兰',
  },

  // production
  production: {
    baseHost: '',
    uploadPath: 'dist/',
    domain: '双兰',
  },
};

export { baseHosts };
