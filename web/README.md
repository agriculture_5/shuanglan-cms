# giibee-cms-web

## Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3001
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

For detailed explanation on how things work, check out the [documentation](https://nuxtjs.org).


## License

[Apache License 2.0](LICENSE).


部署流程：
拉取代码  git pull origin master   
构建代码  yarn build 
启动服务  yarn start
重启服务  pm2 restart nuxt-cms 

